﻿using System;

namespace service.models
{
    public partial class Track
    {
        public int Id { get; set; }
        public string CityName { get; set; }
        public DateTime? Timestamp { get; set; }
        public int? PackageId { get; set; }
    }
}

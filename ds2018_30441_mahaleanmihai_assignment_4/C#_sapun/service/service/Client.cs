using System.Collections.Generic;
using System.Linq;
using service.models;

namespace service
{
    public class Client:IClient
    {
        public List<Package> getMyPackages(int userId)
        {
            A4Context contex=new A4Context();
            var packs = from b in contex.Package
                where b.ReceiverId.Equals(userId)
                select b;
            return packs.ToList();
        }

        public List<Track> getPackTracking(int packId)
        {
            A4Context contex=new A4Context();
            var tracks = from b in contex.Track
                where b.PackageId.Equals(packId)
                select b;
            return tracks.ToList();
        }
        public List<Package> searchPacks(int userId,string criteria)
        {
            A4Context contex=new A4Context();
            var packs = from b in contex.Package
                where b.Description.ToUpper().Contains(criteria.ToUpper()) && b.ReceiverId.Equals(userId)
                select b;
            
            return packs.ToList();
        }
    }
}
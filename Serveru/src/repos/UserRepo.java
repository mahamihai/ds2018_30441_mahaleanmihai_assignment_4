package repos;


import model.User;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;


public class UserRepo extends DAO<User> {
    public User getByUsername(String username)
    {

        List<User> users = new ArrayList<>();
        Transaction tx = null;

        try {
            // Getting Session Object From SessionFactory
            sessionObj = this.sessionFactory.openSession();
            // Getting Transaction Object From Session Object
            tx=sessionObj.beginTransaction();
            CriteriaBuilder builder = sessionObj.getCriteriaBuilder();
            Query query = sessionObj.createQuery("from " + getPersistentClass().getSimpleName()+" as o where o.username=:username");
            query.setParameter("username",username);
            users = query.list();
            users.size();
            tx.commit();
        } catch(Exception sqlException) {
            if(null != sessionObj.getTransaction()) {
                sessionObj.getTransaction().rollback();
            }
            sqlException.printStackTrace();
        } finally {
            if(sessionObj != null) {
                sessionObj.close();
            }
        }
        return users.get(0);
    }

}


package services.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the services.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfPackage_QNAME = new QName("http://tempuri.org/", "ArrayOfPackage");
    private final static QName _Package_QNAME = new QName("http://tempuri.org/", "Package");
    private final static QName _ArrayOfTrack_QNAME = new QName("http://tempuri.org/", "ArrayOfTrack");
    private final static QName _Track_QNAME = new QName("http://tempuri.org/", "Track");
    private final static QName _SearchPacksResponseSearchPacksResult_QNAME = new QName("http://tempuri.org/", "searchPacksResult");
    private final static QName _GetMyPackagesResponseGetMyPackagesResult_QNAME = new QName("http://tempuri.org/", "getMyPackagesResult");
    private final static QName _GetPackTrackingResponseGetPackTrackingResult_QNAME = new QName("http://tempuri.org/", "getPackTrackingResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: services.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfPackage }
     * 
     */
    public ArrayOfPackage createArrayOfPackage() {
        return new ArrayOfPackage();
    }

    /**
     * Create an instance of {@link GetMyPackagesResponse }
     * 
     */
    public GetMyPackagesResponse createGetMyPackagesResponse() {
        return new GetMyPackagesResponse();
    }

    /**
     * Create an instance of {@link SearchPacksResponse }
     * 
     */
    public SearchPacksResponse createSearchPacksResponse() {
        return new SearchPacksResponse();
    }

    /**
     * Create an instance of {@link GetMyPackages }
     * 
     */
    public GetMyPackages createGetMyPackages() {
        return new GetMyPackages();
    }

    /**
     * Create an instance of {@link GetPackTrackingResponse }
     * 
     */
    public GetPackTrackingResponse createGetPackTrackingResponse() {
        return new GetPackTrackingResponse();
    }

    /**
     * Create an instance of {@link ArrayOfTrack }
     * 
     */
    public ArrayOfTrack createArrayOfTrack() {
        return new ArrayOfTrack();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link SearchPacks }
     * 
     */
    public SearchPacks createSearchPacks() {
        return new SearchPacks();
    }

    /**
     * Create an instance of {@link GetPackTracking }
     * 
     */
    public GetPackTracking createGetPackTracking() {
        return new GetPackTracking();
    }

    /**
     * Create an instance of {@link Track }
     * 
     */
    public Track createTrack() {
        return new Track();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfPackage")
    public JAXBElement<ArrayOfPackage> createArrayOfPackage(ArrayOfPackage value) {
        return new JAXBElement<ArrayOfPackage>(_ArrayOfPackage_QNAME, ArrayOfPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Package }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "Package")
    public JAXBElement<Package> createPackage(Package value) {
        return new JAXBElement<Package>(_Package_QNAME, Package.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTrack }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "ArrayOfTrack")
    public JAXBElement<ArrayOfTrack> createArrayOfTrack(ArrayOfTrack value) {
        return new JAXBElement<ArrayOfTrack>(_ArrayOfTrack_QNAME, ArrayOfTrack.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Track }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "Track")
    public JAXBElement<Track> createTrack(Track value) {
        return new JAXBElement<Track>(_Track_QNAME, Track.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "searchPacksResult", scope = SearchPacksResponse.class)
    public JAXBElement<ArrayOfPackage> createSearchPacksResponseSearchPacksResult(ArrayOfPackage value) {
        return new JAXBElement<ArrayOfPackage>(_SearchPacksResponseSearchPacksResult_QNAME, ArrayOfPackage.class, SearchPacksResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getMyPackagesResult", scope = GetMyPackagesResponse.class)
    public JAXBElement<ArrayOfPackage> createGetMyPackagesResponseGetMyPackagesResult(ArrayOfPackage value) {
        return new JAXBElement<ArrayOfPackage>(_GetMyPackagesResponseGetMyPackagesResult_QNAME, ArrayOfPackage.class, GetMyPackagesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfTrack }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "getPackTrackingResult", scope = GetPackTrackingResponse.class)
    public JAXBElement<ArrayOfTrack> createGetPackTrackingResponseGetPackTrackingResult(ArrayOfTrack value) {
        return new JAXBElement<ArrayOfTrack>(_GetPackTrackingResponseGetPackTrackingResult_QNAME, ArrayOfTrack.class, GetPackTrackingResponse.class, value);
    }

}

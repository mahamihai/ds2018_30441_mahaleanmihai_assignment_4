﻿using System.Runtime.Serialization;

namespace service.models
{
    [DataContract]
    public partial class User
    {
        [DataMember] public int Id { get; set; }

        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string Role { get; set; }
        [DataMember]
        public string Username { get; set; }
    }
}

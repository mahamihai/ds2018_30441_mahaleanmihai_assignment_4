package hello.repos;


import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;


public class DAO<T> {
	public DAO() {
		sessionFactory = new Configuration().configure().buildSessionFactory();



	}

	static Session sessionObj;
	protected static SessionFactory sessionFactory;

	public <T> T save(final T o) throws Exception {

	throw  new Exception("Nu e bine");

	}


	public void delete(final Object object) {

		Transaction tx = null;

		try {
			// Getting Session Object From SessionFactory
			sessionObj = sessionFactory.openSession();
			// Getting Transaction Object From Session Object
			tx=sessionObj.beginTransaction();
			sessionObj.delete(object);
			tx.commit();
		} catch(Exception sqlException) {
			if(null != sessionObj.getTransaction()) {
				sessionObj.getTransaction().rollback();
			}
			sqlException.printStackTrace();
		} finally {
			if(sessionObj != null) {
				sessionObj.close();
			}
		}
	}


	/***/
	public <T> T get( final int id) {

		Transaction tx = null;
		T obj=null;
		try {
			// Getting Session Object From SessionFactory
			sessionObj = sessionFactory.openSession();
			// Getting Transaction Object From Session Object
			tx=sessionObj.beginTransaction();
		obj=(T)	sessionObj.find(getPersistentClass(),id);
			tx.commit();
		} catch(Exception sqlException) {
			if(null != sessionObj.getTransaction()) {
				sessionObj.getTransaction().rollback();
			}
			sqlException.printStackTrace();
		} finally {
			if(sessionObj != null) {
				sessionObj.close();
			}
		}
		return obj;
	}

	/***/
	public <T> T merge(final T o) {
		return (T) sessionFactory.getCurrentSession().merge(o);
	}
	public Class<T> getPersistentClass()
	{
		return  (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
	}

	/***/
	public <T> void saveOrUpdate(final T o) {

		Transaction tx = null;
		try {
			// Getting Session Object From SessionFactory
			sessionObj = sessionFactory.openSession();
			// Getting Transaction Object From Session Object
			tx=sessionObj.beginTransaction();
			final Session session =sessionObj;
			session.saveOrUpdate(o);
			tx.commit();
		} catch(Exception sqlException) {
			if(null != sessionObj.getTransaction()) {
				sessionObj.getTransaction().rollback();
			}
			sqlException.printStackTrace();
		} finally {

			if(sessionObj != null) {

				sessionObj.close();
			}
		}
	;
	}

	public  List<T> getAll() {
		List studentsList = new ArrayList<>();
		Transaction tx = null;

		try {
			// Getting Session Object From SessionFactory
			sessionObj = sessionFactory.openSession();
			// Getting Transaction Object From Session Object
			tx=sessionObj.beginTransaction();
			CriteriaBuilder builder = sessionObj.getCriteriaBuilder();
			Query query = sessionObj.createQuery("from " + getPersistentClass().getSimpleName());

			studentsList = query.list();
			studentsList.size();
			tx.commit();
		} catch(Exception sqlException) {
			if(null != sessionObj.getTransaction()) {
				sessionObj.getTransaction().rollback();
			}
			sqlException.printStackTrace();
		} finally {
			if(sessionObj != null) {
				sessionObj.close();
			}
		}
		return studentsList;
	}
}

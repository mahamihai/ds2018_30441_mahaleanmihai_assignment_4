using System.ServiceModel;
using service.models;

namespace service
{
    [ServiceContract]

    public interface IService
    {
        [OperationContract]
        string Ping(string msg);
        [OperationContract]
        User login(string username,string password);
    }
}
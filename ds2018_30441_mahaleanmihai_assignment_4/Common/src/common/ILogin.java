package common;


import model.User;


public interface ILogin {
	User checkUser(String username, String password);

}

package gui;

import common.HelloWorld;
import javafx.scene.control.Alert;
import org.modelmapper.ModelMapper;
import services.login.IService;
import services.login.User;

import javax.swing.*;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;

public class LoginForm {
    private JPanel panel1;

    public void tryLogin(String username,String password) {
        URL url = null;
        try {
            url = new URL("http://localhost:5050/Service.svc");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //1st argument service URI, refer to wsdl document above
        //2nd argument is service name, refer to wsdl document above
        QName qname = new QName("http://tempuri.org/", "IService");
        Service service = Service.create(url, qname);
        IService hello = service.getPort(IService.class);
        User found = hello.login(username, password);
        if(found==null)
        {
            JOptionPane.showMessageDialog(null,                                   "User not found" );

        }
        switch (found.getRole())
        {
            case "Admin":
                (new AdminForm()).init();
                break;
            case "Client":
                ModelMapper mapper=new ModelMapper();
                (new ClientForm(mapper.map(found, model.User.class))).init();
                break;
            default:
                break;
        }
           }
    public LoginForm()
    {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JLabel usernameLabel=new JLabel("Username");
        JTextField userText=new JTextField();
        JLabel passwordLabel=new JLabel("Password");
        JTextField passwordText=new JTextField();
        JButton loginButton=new JButton("Login");
        loginButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String password=passwordText.getText().toString();

                String username=userText.getText().toString();
                tryLogin(username,password);
            }
        });

        panel.add(usernameLabel);
        panel.add(userText);
        panel.add(passwordLabel);
        panel.add(passwordText);
        panel.add(loginButton);

        JFrame login_frame = new JFrame("Login");

        login_frame.pack();        login_frame.setContentPane(panel);

        login_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        login_frame.setSize(1000, 500);
        login_frame.setLocation(300, 300); // setup the fra
        login_frame.setVisible(true);

    }
}

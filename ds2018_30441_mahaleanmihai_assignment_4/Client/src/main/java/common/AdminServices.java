package common;

import gui.AdminForm;
import model.PackageAPI;
import model.User;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AdminServices {


    public List<PackageAPI> getPackages()
    {
        URL url = null;
        try {
            url = new URL("http://localhost:8080/Serveru_war_exploded/AdminService");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        QName qname = new QName("http://endpoints/", "AdminService");
        Service service= Service.create(url,qname);
        IAdmin admin=service.getPort(IAdmin.class);



        List<PackageAPI> allPacks= Arrays.asList(admin.allPacks());
        return allPacks;
        }
        public void removePack(PackageAPI pack)
        {
            URL url = null;
            try {
                url = new URL("http://localhost:8080/Serveru_war_exploded/AdminService");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            QName qname = new QName("http://endpoints/", "AdminService");
            Service service= Service.create(url,qname);
            IAdmin admin=service.getPort(IAdmin.class);
            // admin.registerForTracking(new Package());
            admin.removePackage(pack);
        }
        public void startTracking(PackageAPI pack)
        {
            URL url = null;
            try {
                url = new URL("http://localhost:8080/Serveru_war_exploded/AdminService");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            QName qname = new QName("http://endpoints/", "AdminService");
            Service service= Service.create(url,qname);
            IAdmin admin=service.getPort(IAdmin.class);
            // admin.registerForTracking(new Package());
            admin.registerForTracking(pack);
        }
    public void startTracking(PackageAPI pack,String date, String city)
    {
        URL url = null;
        try {
            url = new URL("http://localhost:8080/Serveru_war_exploded/AdminService");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
        Date formattedDate = null;
        try {
            formattedDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        QName qname = new QName("http://endpoints/", "AdminService");
        Service service= Service.create(url,qname);
        IAdmin admin=service.getPort(IAdmin.class);
        // admin.registerForTracking(new Package());
        admin.addPackageEntry(pack,formattedDate,city);
    }
    public List<User> getUsers()
    {
        URL url = null;
        try {
            url = new URL("http://localhost:8080/Serveru_war_exploded/AdminService");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        QName qname = new QName("http://endpoints/", "AdminService");
        Service service= Service.create(url,qname);
        IAdmin admin=service.getPort(IAdmin.class);
        // admin.registerForTracking(new Package());
        AdminForm client=new AdminForm();


        List<User> allUsers= Arrays.asList(admin.getUsers());
        return allUsers;
    }
    public void addPackage(PackageAPI pack)
    {
        URL url = null;
        try {
            url = new URL("http://localhost:8080/Serveru_war_exploded/AdminService");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //1st argument service URI, refer to wsdl document above
        //2nd argument is service name, refer to wsdl document above
        QName qname = new QName("http://endpoints/", "AdminService");
        Service service = Service.create(url, qname);

        IAdmin admin = service.getPort(IAdmin.class);
        admin.addPackage( pack);
    }
}


package services.client;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.7-b01 
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "IClient", targetNamespace = "http://tempuri.org/", wsdlLocation = "http://localhost:5050/Client.svc")
public class IClient_Service
    extends Service
{

    private final static URL ICLIENT_WSDL_LOCATION;
    private final static WebServiceException ICLIENT_EXCEPTION;
    private final static QName ICLIENT_QNAME = new QName("http://tempuri.org/", "IClient");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:5050/Client.svc");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        ICLIENT_WSDL_LOCATION = url;
        ICLIENT_EXCEPTION = e;
    }

    public IClient_Service() {
        super(__getWsdlLocation(), ICLIENT_QNAME);
    }

    public IClient_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), ICLIENT_QNAME, features);
    }

    public IClient_Service(URL wsdlLocation) {
        super(wsdlLocation, ICLIENT_QNAME);
    }

    public IClient_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, ICLIENT_QNAME, features);
    }

    public IClient_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public IClient_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns IClient
     */
    @WebEndpoint(name = "BasicHttpBinding_IClient")
    public IClient getBasicHttpBindingIClient() {
        return super.getPort(new QName("http://tempuri.org/", "BasicHttpBinding_IClient"), IClient.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns IClient
     */
    @WebEndpoint(name = "BasicHttpBinding_IClient")
    public IClient getBasicHttpBindingIClient(WebServiceFeature... features) {
        return super.getPort(new QName("http://tempuri.org/", "BasicHttpBinding_IClient"), IClient.class, features);
    }

    private static URL __getWsdlLocation() {
        if (ICLIENT_EXCEPTION!= null) {
            throw ICLIENT_EXCEPTION;
        }
        return ICLIENT_WSDL_LOCATION;
    }

}

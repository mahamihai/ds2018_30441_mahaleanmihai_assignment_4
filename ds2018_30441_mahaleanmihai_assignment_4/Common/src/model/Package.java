package model;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name="package")
@XmlRootElement
@XmlSeeAlso({Package.class})
public class Package  implements Serializable {
	@OneToOne(fetch = FetchType.EAGER)
	private User sender;
	@OneToOne(fetch = FetchType.EAGER)

	private User receiver;

		@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}
	public List<Track> getTrack() {
		return track;
	}

	public void setTrack(List<Track> track) {
		this.track = track;
	}

//	@ManyToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "user_id")
//	private User user;


	@OneToMany(
			mappedBy = "thePackage",
			cascade = CascadeType.ALL,
			orphanRemoval = true,
			fetch = FetchType.EAGER
	)
	private List<Track> track=new ArrayList();



	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public boolean isTracking() {
		return tracking;
	}

	public void setTracking(boolean tracking) {
		this.tracking = tracking;
	}



	private String description;





	private boolean tracking=false;


	public String getSenderCity() {
		return senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}

	public String getReceiverCity() {
		return receiverCity;
	}

	public void setReceiverCity(String receiverCity) {
		this.receiverCity = receiverCity;
	}

	private String senderCity;
	private String receiverCity;

}

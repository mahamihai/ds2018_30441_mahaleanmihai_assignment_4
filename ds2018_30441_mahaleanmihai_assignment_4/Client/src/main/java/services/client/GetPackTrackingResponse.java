
package services.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getPackTrackingResult" type="{http://tempuri.org/}ArrayOfTrack" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPackTrackingResult"
})
@XmlRootElement(name = "getPackTrackingResponse")
public class GetPackTrackingResponse {

    @XmlElementRef(name = "getPackTrackingResult", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfTrack> getPackTrackingResult;

    /**
     * Gets the value of the getPackTrackingResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfTrack }{@code >}
     *     
     */
    public JAXBElement<ArrayOfTrack> getGetPackTrackingResult() {
        return getPackTrackingResult;
    }

    /**
     * Sets the value of the getPackTrackingResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfTrack }{@code >}
     *     
     */
    public void setGetPackTrackingResult(JAXBElement<ArrayOfTrack> value) {
        this.getPackTrackingResult = value;
    }

}

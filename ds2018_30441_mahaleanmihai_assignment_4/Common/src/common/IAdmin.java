package common;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.*;
import model.Package;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;


@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IAdmin  {
	@WebMethod void addPackage(PackageAPI p);
	@WebMethod 	void registerForTracking(PackageAPI pack);
	@WebMethod void removePackage(PackageAPI p);
	@WebMethod void addPackageEntry(PackageAPI pack, Date date, String city);
	@WebMethod  PackageAPI[] allPacks();
	@WebMethod  ArrayList<Package> allPacksList();
	@WebMethod 	void deletePackage(Package pack);
	@WebMethod User[] getUsers();
}

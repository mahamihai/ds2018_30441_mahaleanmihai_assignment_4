﻿namespace service.models
{
    public partial class Package
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string ReceiverCity { get; set; }
        public string SenderCity { get; set; }
        public short Tracking { get; set; }
        public int? ReceiverId { get; set; }
        public int? SenderId { get; set; }
    }
}

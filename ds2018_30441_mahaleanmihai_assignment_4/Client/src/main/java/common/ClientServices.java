package common;

import com.sun.xml.bind.v2.runtime.reflect.Lister;
import model.Package;
import model.PackageAPI;
import model.Track;
import model.User;
import org.modelmapper.ModelMapper;
import services.client.ArrayOfPackage;
import services.client.IClient;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClientServices {

    public List<PackageAPI> getPackages(User me)
    {
        URL url = null;
        try {
            url = new URL("http://localhost:5050/Client.svc");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        QName qname = new QName("http://tempuri.org/", "IClient");
        Service service= Service.create(url,qname);
        IClient admin=service.getPort(IClient.class);


        ModelMapper mapper=new ModelMapper();

        List<?> arrayPack = admin.getMyPackages(me.getId()).getPackage();
        List<PackageAPI> convertedPacks=new ArrayList<>();
        for(Object aux:arrayPack)
        {
            convertedPacks.add(mapper.map(aux,PackageAPI.class));
        }
        return convertedPacks;
    }

    public List<PackageAPI> searchForPacks(User me, String criteria)
    {
        URL url = null;
        try {
            url = new URL("http://localhost:5050/Client.svc");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        QName qname = new QName("http://tempuri.org/", "IClient");
        Service service= Service.create(url,qname);
        IClient admin=service.getPort(IClient.class);


        ModelMapper mapper=new ModelMapper();


        List<services.client.Package> allPacks= admin.searchPacks(me.getId(),criteria).getPackage();
        List<PackageAPI> convertedPacks=new ArrayList<>();
        for(services.client.Package aux:allPacks)
        {
            convertedPacks.add(mapper.map(aux,PackageAPI.class));
        }
        return convertedPacks;
    }
    public List<Track> getPackageRoute(PackageAPI pack)
    {
        URL url = null;
        try {
            url = new URL("http://localhost:5050/Client.svc");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        QName qname = new QName("http://tempuri.org/", "IClient");
        Service service= Service.create(url,qname);
        IClient admin=service.getPort(IClient.class);


        ModelMapper mapper=new ModelMapper();


        List<services.client.Track> allTracks= admin.getPackTracking(pack.getId()).getTrack();
        List<Track> convertedPacks=new ArrayList<>();
        for(services.client.Track aux:allTracks)
        {
            convertedPacks.add(mapper.map(aux,Track.class));
        }
        return convertedPacks;
    }

}

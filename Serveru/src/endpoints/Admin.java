package endpoints;

import common.*;

import model.*;
import model.Package;
import org.modelmapper.ModelMapper;
import repos.PackageRepo;
import repos.TrackRepo;
import repos.UserRepo;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@WebService(name="Admin",endpointInterface = "common.IAdmin")
public  class Admin implements IAdmin {

	private PackageRepo _packageRepo=new PackageRepo();
	private TrackRepo _trackRepo=new TrackRepo();
	private UserRepo _userRepo=new UserRepo();
	ModelMapper modelMapper = new ModelMapper();

	@Override public void addPackage(PackageAPI p) {
		Package pack=modelMapper.map(p,Package.class);
		User sender=_userRepo.getByUsername(p.getSender());
		User receiver=_userRepo.getByUsername(p.getReceiver());
		pack.setSender(sender);
		pack.setId(0);
		pack.setReceiver(receiver);

			this._packageRepo.saveOrUpdate(pack);

	}

	@Override public void registerForTracking(PackageAPI pack) {



        Package found=_packageRepo.get(pack.getId());
		found.setTracking(true);
		found.setTrack(new ArrayList<>());
		this._packageRepo.saveOrUpdate(found);

	}

	@Override public void removePackage(PackageAPI pack) {

		Package found=_packageRepo.get(pack.getId());

		this._packageRepo.delete(found);
	}


	@Override public void addPackageEntry(PackageAPI pack, Date date, String city) {
		int id=pack.getId();
		Package found=_packageRepo.get(id);
		Track newTrack=new Track();
		newTrack.setCityName(city);
		newTrack.setTimestamp(date);
		Package converted=modelMapper.map(pack,Package.class);
newTrack.setThePackage(converted);
_packageRepo.saveOrUpdate(pack);

	}

	@Override public PackageAPI[] allPacks() {
		ArrayList<Package> all= (ArrayList<Package>) this._packageRepo.getAll();
		PackageAPI[] allPacks=new PackageAPI[all.size()];
		for(int i=0;i<all.size();i++)
		{
			Package aux=all.get(i);
			allPacks[i]=modelMapper.map(aux,PackageAPI.class);
			try {
				allPacks[i].setReceiverCity(aux.getReceiver().getUsername());
			}
			catch(Exception e)
				{
					System.out.println("Not valid");
				}
			try {
				allPacks[i].setReceiver(aux.getReceiver().getUsername());
			}
			catch(Exception e)
			{
				System.out.println("Not valid");
			}
			try {
				allPacks[i].setSender(aux.getSender().getUsername());
			}
			catch(Exception e)
			{
				System.out.println("Not valid");
			}



		}
		 return allPacks;
	}
	@Override public ArrayList<Package> allPacksList() {
		ArrayList<Package> all= (ArrayList<Package>) this._packageRepo.getAll();
		return all;
	}

	@Override public void deletePackage(Package pack) {
			this._packageRepo.delete(pack);
	}

	@Override
	public User[] getUsers() {
	    List<User> all= this._userRepo.getAll();
        User[] users=new User[all.size()];
        for(int i=0;i<all.size();i++)
        {
            users[i]=all.get(i);
        }
        return users;

	}

	public Admin() {

	}
}

package gui;


import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;
import common.AdminServices;
import common.HelloWorld;
import common.IAdmin;
import model.Package;
import model.PackageAPI;
import model.User;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class AdminForm {

	private DefaultTableModel create_model(List<?> list) {

		int nr_fields = 0;
		// Create a couple of columns
		ArrayList<String> colls = new ArrayList<String>();

		if (list.size() > 0) {
			for (Field field : list.get(0).getClass().getDeclaredFields()) {
				field.setAccessible(true);

				nr_fields++;
				colls.add(field.getName());

			}
		}
		DefaultTableModel model = new DefaultTableModel(colls.toArray(), 0);

		for (Object aux : list) {
			int current = 0;
			Object values[] = new Object[nr_fields];
			for (Field field : aux.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				try {
					field.setAccessible(true);
					String val =(field.get(aux)!=null)? field.get(aux).toString():"";
					values[current] = val;
					current++;
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			model.addRow(values);
		}

		return model;

	}
	protected void setup_info(List<JTextField> views, JTable table) {
		int selected = table.getSelectedRow();

		for (int i = 0; i < table.getColumnCount(); i++) {
			views.get(i).setText((table.getValueAt(selected, i)!=null)?table.getValueAt(selected, i).toString():"");
		}

	}
	private Object create_entry(List<JTextField> views, Class type) {
		Object aux = null;
		//System.out.println(type.getSimpleName());
		try {
			aux = type.newInstance();
		} catch (InstantiationException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IllegalAccessException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		int i = 0;
		for (Field f : type.getDeclaredFields()) {
			System.out.println(f.getName());
			f.setAccessible(true);
			try {
				if(views.get(i).getText()!=null && !views.get(i).getText().toString().equals("")) {
					f.set(aux, Integer.parseInt(views.get(i).getText().toString()));
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				try {
					f.set(aux, views.get(i).getText().toString());
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					try {
						f.set(aux, Boolean.valueOf(views.get(i).getText().toString()));
					} catch (IllegalAccessException e2) {
						e2.printStackTrace();
					}

				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block

				}

			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block

			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block

			}
			i++;
		}

		return aux;
	}
	private ArrayList<JTextField> create_fields(Class type, JPanel panel) {
		ArrayList<JTextField> display_fields = new ArrayList<JTextField>();
		for (Field field : type.getDeclaredFields()) // add labels to display
		// info for editing

		{
			JLabel tmp = new JLabel(field.getName());
			panel.add(tmp);
			JTextField aux = new JTextField();
			aux.setMinimumSize(new Dimension(100, 30));
			aux.setPreferredSize(new Dimension(100, 30));
			display_fields.add(aux);
			panel.add(aux);

		}
		return display_fields;

	}
	public List<JTextField> fields;
	public JTable table;
	private AdminServices _admin=new AdminServices();
	public void refreshPackages()
    {
        TableModel newModel=new TableModel(PackageAPI.class,_admin.getPackages());
        this.table.setModel(newModel);
        newModel.fireTableDataChanged();
    }
    public void enableTracking()
    {
        Object fieldObj=create_entry(fields,PackageAPI.class);
        _admin.startTracking((PackageAPI) fieldObj);
    }
    public void addTracking(String date, String city)
    {
        Object fieldObj=create_entry(fields,PackageAPI.class);
        _admin.startTracking((PackageAPI) fieldObj,date,city);
    }
	public void init()
	{
		List<PackageAPI> in=_admin.getPackages();
		JTable table=new JTable(create_model(in));
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JFrame client_frame = new JFrame("Client");
		client_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		client_frame.setSize(1000, 500);
		client_frame.setLocation(300, 300); // setup the fra
		//panel.add(table);
		//client_frame.add(panel);




		TableModel<PackageAPI> model = new TableModel<PackageAPI>(PackageAPI.class,in);
		JScrollPane scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		table.setModel(model);
		this.table=table;
		table.addMouseListener(new java.awt.event.MouseAdapter() {
		@Override
		public void mouseClicked(java.awt.event.MouseEvent evt) {
			int row = table.rowAtPoint(evt.getPoint());
			int col = table.columnAtPoint(evt.getPoint());
			if (row >= 0 && col >= 0) {
          		setup_info(fields,table);


			}
		}
	});
		JButton add=new JButton("Add");
		add.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				Object fieldObj=create_entry(fields,PackageAPI.class);
				_admin.addPackage((PackageAPI) fieldObj);
                refreshPackages();
			}
		}
		);
		JButton delete=new JButton("Delete");
		delete.addActionListener(new ActionListener() {
								  @Override
								  public void actionPerformed(ActionEvent actionEvent) {
									  Object fieldObj=create_entry(fields,PackageAPI.class);
									  _admin.removePack((PackageAPI) fieldObj);
                                      refreshPackages();
								  }
							  }
		);
		JButton trackB=new JButton("Enable tracking");
		trackB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                enableTracking();
                refreshPackages();
            }
        });
		JLabel cityLabel=new JLabel("City:");
		JLabel dateLabel=new JLabel("Date:");
		JTextField cityInput=new JTextField();
		JTextField dateInput=new JTextField();
		JButton addTrack=new JButton("Add location");
        addTrack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addTracking(dateInput.getText().toString(),cityInput.getText().toString());
                refreshPackages();

            }
        });
		JTable userTable=new JTable();
		List<User> users=(new AdminServices()).getUsers();

		TableModel<User> userModel = new TableModel<User>(User.class,users);
		userTable=new JTable(create_model(users));
		panel.add(userTable);

		panel.add(scrollPane, BorderLayout.CENTER);
		client_frame.add(panel);
		fields=create_fields(PackageAPI.class,panel);
		panel.add(add);
		panel.add(delete);
		panel.add(trackB);
		panel.add(cityLabel);
		panel.add(cityInput);
		panel.add(dateLabel);
		panel.add(dateInput);
		panel.add(addTrack);

		client_frame.pack();
		client_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		client_frame.setVisible(true);

		JLabel label=new JLabel("Name");

	}
}

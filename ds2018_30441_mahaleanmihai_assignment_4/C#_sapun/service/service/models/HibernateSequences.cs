﻿namespace service.models
{
    public partial class HibernateSequences
    {
        public string SequenceName { get; set; }
        public long? NextVal { get; set; }
    }
}


package services.login;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.7-b01 
 * Generated source version: 2.2
 * 
 */
@WebService(name = "IService", targetNamespace = "http://tempuri.org/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface IService {


    /**
     * 
     * @param msg
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "Ping", action = "http://tempuri.org/IService/Ping")
    @WebResult(name = "PingResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "Ping", targetNamespace = "http://tempuri.org/", className = "services.login.Ping")
    @ResponseWrapper(localName = "PingResponse", targetNamespace = "http://tempuri.org/", className = "services.login.PingResponse")
    public String ping(
        @WebParam(name = "msg", targetNamespace = "http://tempuri.org/")
        String msg);

    /**
     * 
     * @param password
     * @param username
     * @return
     *     returns services.login.User
     */
    @WebMethod(action = "http://tempuri.org/IService/login")
    @WebResult(name = "loginResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "login", targetNamespace = "http://tempuri.org/", className = "services.login.Login")
    @ResponseWrapper(localName = "loginResponse", targetNamespace = "http://tempuri.org/", className = "services.login.LoginResponse")
    public User login(
        @WebParam(name = "username", targetNamespace = "http://tempuri.org/")
        String username,
        @WebParam(name = "password", targetNamespace = "http://tempuri.org/")
        String password);

}

package model;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name="user")
@XmlRootElement
@XmlSeeAlso({User.class})
public class User implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	public User(String username, String password, String role) {
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public User() {
	}

//	public List<Package> getPackages() {
//		return packages;
//	}
//
//	public void setPackages(List<Package> packages) {
//		this.packages = packages;
//	}

	private String username;
	private String password;
	private String role;

//	@OneToMany(
//			mappedBy = "user",
//			cascade = CascadeType.ALL,
//			orphanRemoval = true
//	)
//
//	private List<Package> packages;

}

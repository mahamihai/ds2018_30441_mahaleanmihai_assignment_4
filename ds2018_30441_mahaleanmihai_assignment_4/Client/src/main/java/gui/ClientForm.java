package gui;

import common.AdminServices;
import common.ClientServices;
import model.PackageAPI;
import model.Track;
import model.User;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ClientForm {

    public ClientForm(User user)
    {
        this.currentUser=user;
    }
    private DefaultTableModel create_model(List<?> list) {

        int nr_fields = 0;
        // Create a couple of columns
        ArrayList<String> colls = new ArrayList<String>();

        if (list.size() > 0) {
            for (Field field : list.get(0).getClass().getDeclaredFields()) {
                field.setAccessible(true);

                nr_fields++;
                colls.add(field.getName());

            }
        }
        DefaultTableModel model = new DefaultTableModel(colls.toArray(), 0);

        for (Object aux : list) {
            int current = 0;
            Object values[] = new Object[nr_fields];
            for (Field field : aux.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                try {
                    field.setAccessible(true);
                    String val =(field.get(aux)!=null)? field.get(aux).toString():"";
                    values[current] = val;
                    current++;
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
            model.addRow(values);
        }

        return model;

    }
    protected void setup_info(List<JTextField> views, JTable table) {
        int selected = table.getSelectedRow();

        for (int i = 0; i < table.getColumnCount(); i++) {
            views.get(i).setText((table.getValueAt(selected, i)!=null)?table.getValueAt(selected, i).toString():"");
        }

    }
    private Object create_entry(List<JTextField> views, Class type) {
        Object aux = null;
        //System.out.println(type.getSimpleName());
        try {
            aux = type.newInstance();
        } catch (InstantiationException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        } catch (IllegalAccessException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }

        int i = 0;
        for (Field f : type.getDeclaredFields()) {
            System.out.println(f.getName());
            f.setAccessible(true);
            try {
                if(views.get(i).getText()!=null && !views.get(i).getText().toString().equals("")) {
                    f.set(aux, Integer.parseInt(views.get(i).getText().toString()));
                }
            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
                try {
                    f.set(aux, views.get(i).getText().toString());
                } catch (IllegalArgumentException e1) {
                    // TODO Auto-generated catch block
                    try {
                        f.set(aux, Boolean.valueOf(views.get(i).getText().toString()));
                    } catch (IllegalAccessException e2) {
                        e2.printStackTrace();
                    }

                } catch (IllegalAccessException e1) {
                    // TODO Auto-generated catch block

                }

            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block

            } catch (IllegalAccessException e) {
                // TODO Auto-generated catch block

            }
            i++;
        }

        return aux;
    }
    private ArrayList<JTextField> create_fields(Class type, JPanel panel) {
        ArrayList<JTextField> display_fields = new ArrayList<JTextField>();
        for (Field field : type.getDeclaredFields()) // add labels to display
        // info for editing

        {
            JLabel tmp = new JLabel(field.getName());
            panel.add(tmp);
            JTextField aux = new JTextField();
            aux.setMinimumSize(new Dimension(50, 30));
            aux.setPreferredSize(new Dimension(50, 30));
            display_fields.add(aux);
            panel.add(aux);

        }
        return display_fields;

    }
    public List<JTextField> fields;
    public JTable packsTable;
    public JTable trackingTable;
    private ClientServices _client=new ClientServices();
    private User currentUser;
    private JFrame frame;
    public void searchPackages(String criteria)
    {
        List<?> packsList=new ArrayList<>();
        if(criteria==null || criteria.equals(""))
        {
            packsList=_client.getPackages(this.currentUser);


        }
        else
        {
            packsList=_client.searchForPacks(this.currentUser,criteria);
        }
        TableModel<PackageAPI> packageModel=new TableModel<>(PackageAPI.class,packsList);
        packsTable.setModel(packageModel);

        packageModel.fireTableDataChanged();


    }
    public void init() {
        this.packsTable=new JTable();
        this.trackingTable=new JTable();
        packsTable.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                int row = packsTable.rowAtPoint(evt.getPoint());
                int col = packsTable.columnAtPoint(evt.getPoint());
                if (row >= 0 && col >= 0) {
                    setup_info(fields,packsTable);
                }
                PackageAPI selectedPack= (PackageAPI) create_entry(fields,PackageAPI.class);

                List<Track> packageTracks=_client.getPackageRoute(selectedPack);
                TableModel<Track> trackModel=new TableModel<Track>(Track.class,packageTracks);
                trackingTable.setModel(trackModel);
                trackModel.fireTableDataChanged();
            }
        });
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JLabel criteriaLabel=new JLabel("Search by:");
        JTextField criteriaInput=new JTextField("");
        criteriaInput.setMinimumSize(new Dimension(100, 30));
        criteriaInput.setPreferredSize(new Dimension(100, 30));
        JButton searchButton=new JButton("SEARCH");
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String criteria=criteriaInput.getText().toString();
                searchPackages(criteria);
            }
        });
        JLabel trackLabel=new JLabel("Tracking");



        panel.add(criteriaLabel);
        panel.add(criteriaInput);
        panel.add(searchButton);
        panel.add(this.packsTable);
        panel.add(trackLabel);
        panel.add(this.trackingTable);
        JPanel inputPanel=new JPanel();
        this.fields= create_fields(PackageAPI.class,inputPanel);

        panel.add(inputPanel);

        panel.add(new JLabel("dsada"));
        frame = new JFrame("Client");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 800);
        frame.setLocation(300, 300); // setup the fra
        frame.add(panel);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);




    }
}

﻿using Microsoft.EntityFrameworkCore;

namespace service.models
{
    public partial class A4Context : DbContext
    {
        public A4Context()
        {
        }

        public A4Context(DbContextOptions<A4Context> options)
            : base(options)
        {
        }

        public virtual DbSet<HibernateSequences> HibernateSequences { get; set; }
        public virtual DbSet<Package> Package { get; set; }
        public virtual DbSet<Track> Track { get; set; }
        public virtual DbSet<User> User { get; set; }

        // Unable to generate entity type for table 'A4.hibernate_sequence'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySQL("server=localhost;uid=root;pwd=root;database=A4");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<HibernateSequences>(entity =>
            {
                entity.HasKey(e => e.SequenceName);

                entity.ToTable("hibernate_sequences", "A4");

                entity.Property(e => e.SequenceName)
                    .HasColumnName("sequence_name")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.NextVal)
                    .HasColumnName("next_val")
                    .HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<Package>(entity =>
            {
                entity.ToTable("package", "A4");

                entity.HasIndex(e => e.ReceiverId)
                    .HasName("FKlog8q72um5cjcb42tksv9cc0k");

                entity.HasIndex(e => e.SenderId)
                    .HasName("FK8awk4mo9i0gw0lj0j6hr2rdm1");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiverCity)
                    .HasColumnName("receiverCity")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiverId)
                    .HasColumnName("receiver_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SenderCity)
                    .HasColumnName("senderCity")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SenderId)
                    .HasColumnName("sender_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Tracking)
                    .HasColumnName("tracking")
                    .HasColumnType("bit(1)");
            });

            modelBuilder.Entity<Track>(entity =>
            {
                entity.ToTable("track", "A4");

                entity.HasIndex(e => e.PackageId)
                    .HasName("FKeoms1rqhfpqbo6bjeqfg8mhg9");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.CityName)
                    .HasColumnName("cityName")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PackageId)
                    .HasColumnName("package_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Timestamp).HasColumnName("timestamp");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user", "A4");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Role)
                    .HasColumnName("role")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
        }
    }
}

using System.Collections.Generic;
using System.ServiceModel;
using service.models;

namespace service
{
    [ServiceContract]
    public interface IClient
    {
        [OperationContract]
        List<Package> getMyPackages(int userId);
        [OperationContract]
        List<Package> searchPacks(int userId,string criteria);

        [OperationContract]
         List<Track> getPackTracking(int packId);
    }
}
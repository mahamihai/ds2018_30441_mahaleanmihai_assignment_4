
package services.login;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.7-b01 
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "IService", targetNamespace = "http://tempuri.org/", wsdlLocation = "http://localhost:5050/Service.svc")
public class IService_Service
    extends Service
{

    private final static URL ISERVICE_WSDL_LOCATION;
    private final static WebServiceException ISERVICE_EXCEPTION;
    private final static QName ISERVICE_QNAME = new QName("http://tempuri.org/", "IService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:5050/Service.svc");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        ISERVICE_WSDL_LOCATION = url;
        ISERVICE_EXCEPTION = e;
    }

    public IService_Service() {
        super(__getWsdlLocation(), ISERVICE_QNAME);
    }

    public IService_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), ISERVICE_QNAME, features);
    }

    public IService_Service(URL wsdlLocation) {
        super(wsdlLocation, ISERVICE_QNAME);
    }

    public IService_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, ISERVICE_QNAME, features);
    }

    public IService_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public IService_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns IService
     */
    @WebEndpoint(name = "BasicHttpBinding_IService")
    public IService getBasicHttpBindingIService() {
        return super.getPort(new QName("http://tempuri.org/", "BasicHttpBinding_IService"), IService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns IService
     */
    @WebEndpoint(name = "BasicHttpBinding_IService")
    public IService getBasicHttpBindingIService(WebServiceFeature... features) {
        return super.getPort(new QName("http://tempuri.org/", "BasicHttpBinding_IService"), IService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (ISERVICE_EXCEPTION!= null) {
            throw ISERVICE_EXCEPTION;
        }
        return ISERVICE_WSDL_LOCATION;
    }

}

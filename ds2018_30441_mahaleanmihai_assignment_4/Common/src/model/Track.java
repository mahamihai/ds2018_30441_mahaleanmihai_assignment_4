package model;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="track")
@XmlRootElement
@XmlSeeAlso({Track.class})
public class Track implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String cityName;

	public Track() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	private Date timestamp;

	public Package getThePackage() {
		return thePackage;
	}

	public void setThePackage(Package thePackage) {
		this.thePackage = thePackage;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "package_id")
	private Package thePackage;


}

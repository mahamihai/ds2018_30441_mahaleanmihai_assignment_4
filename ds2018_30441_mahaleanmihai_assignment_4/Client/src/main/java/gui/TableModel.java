package gui;

import javax.swing.table.AbstractTableModel;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.security.spec.ECField;
import java.util.List;
import model.PackageAPI;
public class TableModel<T> extends AbstractTableModel {

    List<T> wordsList;

    String headerList[] ;
    public void generateHeaders()
    {
       Class m =type.getClass();
        headerList=new String[type.getDeclaredFields().length];
      int index=0;
            for (Field field : type.getDeclaredFields()) {
                field.setAccessible(true);


                headerList[index++]=field.getName();

            }

    }
    private final Class<T> type;

    public  TableModel(Class<T> type,List list) {
    super();
    this.type=type;
        wordsList = list;
    generateHeaders();
        }

@Override
public int getColumnCount() {
        return headerList.length;
        }



@Override
public int getRowCount() {
        return wordsList.size();
        }

// this method is called to set the value of each cell
@Override
public Object getValueAt(int row, int column) {
    T entity = null;
    entity = wordsList.get(row);
try {

    Field f = type.getDeclaredFields()[column];
    f.setAccessible(true);
    return f.get(entity);
}

catch(Exception e)
{

            return "";
    }
}


    public String getColumnName(int col){
        return headerList[col];
    }
}